import praw
from discord_webhook import DiscordWebhook, DiscordEmbed
import time

reddit = praw.Reddit(client_id='a833m5RLpkzYGQ', client_secret='yN53vMGZrfZ3VdLqb9CkGcIcIbo', user_agent='tarkov meme gatherer')
webhook = DiscordWebhook(url='https://canary.discordapp.com/api/webhooks/534953149002285076/HYgWwRRK4vnhif2IZ2AtppzyYfDQEVeP2bK3EZtVLOE_WVl_l88csQ_pZ93O4Xd22jTc')

submissions_seen = []

while True:
    for submission in reddit.subreddit('EscapefromTarkov').hot(limit=20):
        if submission.link_flair_text.lower() == "meme":
            if (submission.id not in submissions_seen) and (submission.score > 10):
                embed = DiscordEmbed(title=submission.title)
                embed.set_image(url=submission.url)
                webhook.add_embed(embed)
                submissions_seen.append(submission.id)

    webhook.execute()
    time.sleep(60)
